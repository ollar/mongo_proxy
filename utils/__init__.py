from aiohttp import web
import json
import jwt
import pymongo
from json.decoder import JSONDecodeError
from bson import ObjectId
from functools import partial
from datetime import datetime

get_path_array = lambda path: path.split('/')


def default_json(o):
    if isinstance(o, ObjectId):
        return str(o)
    if isinstance(o, datetime):
        return o.timestamp() * 1000
    return json.JSONEncoder.default(o)  # pylint: disable=no-value-for-parameter


json_dumps = partial(json.dumps, default=default_json)
json_response = partial(web.json_response, dumps=json_dumps)


def parse_request_path(request):
    if not request.match_info.get('path'):
        return ([], '', None, '')
    path = get_path_array(request.match_info['path'])
    path_array = path[:]

    collection_full_name = ('/').join(path)
    entity_id = path.pop()
    collection_list_name = ('/').join(path)

    return (path_array, collection_full_name, entity_id, collection_list_name)


def validate_object_id(oid):
    return ObjectId.is_valid(oid)


def extract_parameter(param_name, query):
    param = query.get(param_name, '{}')

    try:
        param = json.loads(param)
    except:
        param = {}

    return param

# todo: rewrite
def _handle_sort_props(sort_query):
    default_sorting = ('_id', pymongo.DESCENDING)

    def escape_values(key, value):
        if value in [pymongo.ASCENDING, pymongo.DESCENDING]:
            return (key, value)
        else:
            return default_sorting

    if len(sort_query.keys()):
        sort = [escape_values(key, value) for key, value in sort_query.items()]
    else:
        sort = [default_sorting]

    return sort


def _handle_pagination_props(pagination):
    if pagination == {}:
        return None

    try:
        pagination = int(pagination)
    except:
        return None

    return pagination


def parse_query(query):
    find = extract_parameter('find', query)
    sort = _handle_sort_props(extract_parameter('sort', query))
    pagination = _handle_pagination_props(extract_parameter('page', query))

    return find, sort, pagination



def _get_user_from_bearer(bearer, secret):
    return jwt.decode(bearer, secret)


def _user_is_authorized(bearer, secret):
    if not bearer:
        return False

    user = _get_user_from_bearer(bearer, secret)

    if user:
        return True

    return False