SHELL := /bin/bash

stop_mongo:
	-docker stop mongoproxy || true && docker rm mongoproxy || true

start_mongo: stop_mongo
	docker run -d -p 27017:27017 -v ~/mongo_data:/data/db --name mongoproxy mongo

check_mongo_running:
	docker inspect -f '{{.State.Running}}' mongoproxy

dev:
	python run_dev.py

prod:
	gunicorn -D 'app.main:make_app' --workers=3 --bind 0.0.0.0:8080 --worker-class aiohttp.worker.GunicornWebWorker --access-logfile app_access.log --error-logfile app_error.log

test:
	ENV='test' pytest -rs -v

ci-test: start_mongo
	make test


.PHONY: dev
