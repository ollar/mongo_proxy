from .fixtures import client

import json
from bson import ObjectId

from app.errors import INVALID_COLLECTION_NAME

dumps = json.dumps

data = dumps({
    "test": True
})

async def test_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.post('/api/aa', data=data)

    result = await resp.json()
    assert result
    _id = result.get("_id")

    resp = await client.patch(f'/api/aa/{_id}', data=dumps({
        "test": False
    }))

    assert resp.status == 200

async def test_patch_empty(client):
    """
    Tests app rejects patch on empty urls
    """
    resp = await client.patch('/api/', data=data)
    assert resp.status == 400
    assert INVALID_COLLECTION_NAME in await resp.text()

async def test_different_urls(client):
    id = str(ObjectId())

    resp = await client.patch(f'/api/555/{id}', data=data)
    assert resp.status == 200

    resp = await client.patch(f'/api/œ˙´®ß¯ðƒ&/{id}', data=data)
    assert resp.status == 200

    resp = await client.patch(f'/api/не надо так/{id}', data=data)
    assert resp.status == 200

    resp = await client.patch(f'/api/   /{id}', data=data)
    assert resp.status == 200

    resp = await client.patch(f'/api/œ˙´®ß¯ðƒ$//{id}', data=data)
    assert resp.status == 400
    assert INVALID_COLLECTION_NAME in await resp.text()