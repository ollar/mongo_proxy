from json import dumps
from .fixtures import client


post_data = [{
        "test": True,
        "number": i
    } for i in range(100)]


async def test_get_sorting_desc(client):
    # prepare data
    await client.post('/api/aa', data=dumps(post_data))

    # actual tests
    resp = await client.get('/api/aa?sort={"number":-1}')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99


async def test_get_sorting_asc(client):
    # prepare data
    await client.post('/api/aa', data=dumps(post_data))

    resp = await client.get('/api/aa?sort={"number":1}')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 0


async def test_get_sorting_invalid(client):
    # prepare data
    await client.post('/api/aa', data=dumps(post_data))

    # invalid values fall back to acceptable
    resp = await client.get('/api/aa?sort={"number":2}')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99

    # invalid values fall back to acceptable
    resp = await client.get('/api/aa?sort={"number":-2}')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99

    # invalid values fall back to acceptable
    resp = await client.get('/api/aa?sort={"number":"aaa"}')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99
