from json import dumps

from app.errors import INVALID_ENTITY_ID
from .fixtures import client


async def test_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.get('/api/aa')
    assert resp.status == 200

async def test_empty(client):
    """
    Empty url should return 400 error response
    """
    resp = await client.get('/api/')
    assert resp.status == 400

async def test_deep_url(client):
    """
    Deep urls should work
    """
    resp = await client.get('/api/aaa/bb')
    assert resp.status == 200

async def test_aaabbb_url_should_fail_if_aaa_collection_exists(client):
    """
    AAA/BBB url should fail if AAA collection exists
    """
    await client.post('/api/aa', data=dumps({
        "test": True
    }))

    resp = await client.get('/api/aa/bb')
    assert resp.status == 400
    assert INVALID_ENTITY_ID in await resp.text()

async def test_different_urls(client):
    resp = await client.get('/api/555')
    assert resp.status == 200

    resp = await client.get('/api/œ˙´®ß¯ðƒ///!@#$')
    assert resp.status == 200

    resp = await client.get('/api/не надо так')
    assert resp.status == 200

    resp = await client.get('/api/   ')
    assert resp.status == 200
