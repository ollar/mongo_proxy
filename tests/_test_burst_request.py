import asyncio
from json import dumps
import jwt

from .fixtures import client

data_a = lambda x: {"a": x}
data_b = lambda x: {"b": x}


staff = {
    "id": 1,
    "name": "staff",
    "is_staff": True
}


def get_auth(client, user):
    configs = client.app['configs']
    return jwt.encode(user, configs.secret).decode()


async def test_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.get('/burst?query=[]')
    assert resp.status == 400


async def test_gather_by_id(client):
    """
    Tests app burst gather by id
    """
    post_data = [data_a(i) for i in range(10)]

    resps = await client.post('/api/a', data=dumps(post_data))
    resps = await resps.json()

    ids = [item.get('_id') for item in resps]

    resp = await client.get(f'/burst?query={dumps([{"a":{"id":item}} for item in ids])}')
    assert resp.status == 200

    json = await resp.json()

    assert len(json) == 10


async def test_gather_by_id_wrong_entity(client):
    """
    Tests app burst gather by id
    """
    post_data_b = [data_b(i) for i in range(10)]

    resps = await client.post('/api/b', data=dumps([*post_data_b]))
    resps = await resps.json()

    ids = [item.get('_id') for item in resps]

    resp = await client.get(f'/burst?query={dumps([{"a":{"id":item}} for item in ids])}')
    assert resp.status == 200

    json = await resp.json()

    assert len(json) == 0


async def test_gather_by_id_a_b(client):
    """
    Tests app burst gather by id
    """
    post_data_a = [data_a(i) for i in range(10)]
    post_data_b = [data_b(i) for i in range(10)]

    resps_a = await client.post('/api/a', data=dumps([*post_data_a]))
    resps_a = await resps_a.json()

    resps_b = await client.post('/api/b', data=dumps([*post_data_b]))
    resps_b = await resps_b.json()

    ids = [item.get('_id') for item in resps_a]

    resp = await client.get(f'/burst?query={dumps([{"a":{"id":item}} for item in ids])}')
    assert resp.status == 200

    json = await resp.json()

    assert len(json) == 10


    ids = [item.get('_id') for item in resps_b]

    resp = await client.get(f'/burst?query={dumps([{"b":{"id":item}} for item in ids])}')
    assert resp.status == 200

    json = await resp.json()

    assert len(json) == 10

    resp = await client.get(f'/burst?query={dumps([{"a":{"id":item}} for item in ids])}')
    assert resp.status == 200

    json = await resp.json()

    assert len(json) == 0


async def test_gather_by_id_resticted(client):
    resp = await client.post('/api/a', data=dumps(data_a(1)))
    json = await resp.json()

    _id = json.get('_id')

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/a', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get(f'/burst?query={dumps([{"a":{"id":_id}}])}')

    assert resp.status == 403


async def test_gather_by_id_resticted_passed(client):
    resp = await client.post('/api/a', data=dumps(data_a(1)))
    json = await resp.json()

    _id = json.get('_id')

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/a', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get(f'/burst?query={dumps([{"a":{"id":_id}}])}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200


async def test_gather_by_find_resticted(client):
    await client.post('/api/a', data=dumps(data_a(1)))

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/a', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get(f'/burst?query={dumps([{"a":{"find": {"a": 1}}}])}')

    assert resp.status == 403


async def test_gather_by_find_resticted_passed(client):
    resp = await client.post('/api/a', data=dumps(data_a(1)))
    json = await resp.json()

    _id = json.get('_id')

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/a', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get(f'/burst?query={dumps([{"a":{"id":_id}}])}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200