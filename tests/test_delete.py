from .fixtures import client

import json
from bson import ObjectId

from app.errors import INVALID_COLLECTION_NAME, INVALID_ENTITY_ID

dumps = json.dumps

data = dumps({
    "test": True
})

async def test_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.post('/api/aa', data=data)

    result = await resp.text()
    result = json.loads(result)
    _id = result["_id"]

    resp = await client.delete(f'/api/aa/{_id}')

    assert resp.status == 200

async def test_del_empty(client):
    """
    Tests app rejects deletes on empty urls
    """
    resp = await client.delete('/api/')
    assert resp.status == 400
    assert INVALID_COLLECTION_NAME in await resp.text()

async def test_del_no_entity(client):
    """
    Tests app deletes on empty urls
    """
    resp = await client.delete('/api/aaa')
    assert resp.status == 400
    assert INVALID_COLLECTION_NAME in await resp.text()

async def test_del_invalid_id(client):
    """
    Tests app deletes on invalid id
    """
    id = '111'

    resp = await client.delete(f'/api/aaa/{id}')
    assert resp.status == 400
    assert INVALID_ENTITY_ID in await resp.text()

async def test_del_404(client):
    """
    Tests app deletes 404
    """
    id = str(ObjectId())

    resp = await client.delete(f'/api/aaa/{id}')
    # somehow it does not cares
    # TODO: check this later
    assert resp.status == 200

async def test_different_urls(client):
    id = str(ObjectId())

    resp = await client.delete(f'/api/555/{id}')
    assert resp.status == 200

    resp = await client.delete(f'/api/œ˙´®ß¯ðƒ&/{id}')
    assert resp.status == 200

    resp = await client.delete(f'/api/не надо так/{id}')
    assert resp.status == 200

    resp = await client.delete(f'/api/   /{id}')
    assert resp.status == 200

    resp = await client.delete(f'/api/œ˙´®ß¯ðƒ$//{id}')
    assert resp.status == 400
    assert INVALID_COLLECTION_NAME in await resp.text()