from json import dumps
from .fixtures import client


post_data = [{
            "test": True,
            "number": i
        } for i in range(100)]


async def test_get_paginating(client):
    # Prepare test data
    await client.post('/api/aa', data=dumps(post_data))

    # test request without paging
    resp = await client.get('/api/aa')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99


async def test_get_paginating_2(client):
    # Prepare test data
    await client.post('/api/aa', data=dumps(post_data))

    # test paging
    resp = await client.get('/api/aa?page=2')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 89


async def test_get_paginating_3(client):
    # Prepare test data
    await client.post('/api/aa', data=dumps(post_data))

    resp = await client.get('/api/aa?page=3')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 79


async def test_get_paginating_5(client):
    configs = client.app['configs']

    # Prepare test data
    await client.post('/api/aa', data=dumps(post_data))


    resp = await client.get('/api/aa?page=5')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99 - (5 - 1) * configs.list_length


async def test_get_paginating_invalid(client):
    # Prepare test data
    await client.post('/api/aa', data=dumps(post_data))

    # invalid values fall back to acceptable
    resp = await client.get('/api/aa?page=alalal')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99

    # invalid values fall back to acceptable
    resp = await client.get('/api/aa?page="alalal"')
    assert resp.status == 200
    resp_json = await resp.json()
    assert len(resp_json) == 10
    assert resp_json[0]["number"] == 99