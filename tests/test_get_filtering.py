from json import dumps

from .fixtures import client


async def prepare_data(client):
    # Prepare test data
    await client.post('/api/aa', data=dumps({
        "name": "Igor",
        "age": 10
    }))
    await client.post('/api/aa', data=dumps({
        "name": "Megou",
        "age": 30
    }))
    await client.post('/api/aa', data=dumps({
        "name": "Pupi",
        "age": 11
    }))
    await client.post('/api/aa', data=dumps({
        "name": "Sally",
        "age": 20
    }))

async def test_get_filtering_by_name(client):
    await prepare_data(client)

    # test filter
    resp = await client.get('/api/aa?find={"name": "Igor"}')
    assert resp.status == 200
    resp_json = await resp.json()
    resp_text = await resp.text()
    assert len(resp_json) == 1
    assert "Igor" in resp_text


async def test_get_filtering_by_age(client):
    await prepare_data(client)

    resp = await client.get('/api/aa?find={"age": 30}')
    assert resp.status == 200
    resp_json = await resp.json()
    resp_text = await resp.text()
    assert len(resp_json) == 1
    assert "Megou" in resp_text


async def test_get_filtering_complex(client):
    await prepare_data(client)

    resp = await client.get('/api/aa?find={"age": {"$gt": 29}}')
    assert resp.status == 200
    resp_json = await resp.json()
    resp_text = await resp.text()
    assert len(resp_json) == 1
    assert "Megou" in resp_text