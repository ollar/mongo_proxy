from json import dumps
from .fixtures import client
import jwt

data = {
    "test": True,
    "number": 100
}

staff = {
    "_id": 1,
    "name": "staff",
    "is_staff": True
}

user = {
    "_id": 1,
    "name": "tester"
}

user2 = {
    "_id": 2,
    "name": "tester2"
}

def get_auth(client, user):
    configs = client.app['configs']
    return jwt.encode(user, configs.secret).decode()

async def test_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.get('/api/aa')
    assert resp.status == 200


async def test_authorized_restrict(client):
    await client.post('/api/aa', data=dumps(data))

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/aa', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get('/api/aa')

    assert resp.status == 403


async def test_authorized_find_restrict(client):
    await client.post('/api/aa', data=dumps(data))

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/aa', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get('/api/aa?find={"a": 2}')

    assert resp.status == 403


async def test_authorized_allow(client):
    await client.post('/api/aa', data=dumps(data))

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/aa', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    auth = get_auth(client, user)

    resp = await client.get('/api/aa', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200


async def test_authorized_restrict_to_instance(client):
    """
    App should block access to instance by collection rules
    """
    instance = await client.post('/api/aa', data=dumps(data))
    instance = await instance.json()

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/aa', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get(f'/api/aa/{instance["_id"]}')

    assert resp.status == 403


async def test_authorized_allow_to_instance(client):
    """
    App should allow access to instance by collection rules
    """
    instance = await client.post('/api/aa', data=dumps(data))
    instance = await instance.json()

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/aa', data=dumps({
        "GET": "authorized"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    auth = get_auth(client, user)

    resp = await client.get(f'/api/aa/{instance["_id"]}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200


async def test_author_restrict(client):
    _data = dict(**data, _uid=1)

    instance = await client.post('/api/aa', data=dumps(_data))
    instance = await instance.json()

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/aa', data=dumps({
        "GET": "owner"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get(f'/api/aa/{instance["_id"]}')

    assert resp.status == 403

    auth = get_auth(client, dict(**user, user_id=1))

    resp = await client.get(f'/api/aa/{instance["_id"]}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200

    auth = get_auth(client, user2)

    resp = await client.get(f'/api/aa/{instance["_id"]}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 403

    resp = await client.get(f'/api/aa', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200


async def test_root_restrict(client):
    _data = dict(**data, uid=1)

    instance = await client.post('/api/aa', data=dumps(_data))
    instance = await instance.json()

    auth = get_auth(client, staff)

    await client.post('/api/__access_rules__/aa', data=dumps({
        "GET": "staff"
    }), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get(f'/api/aa/{instance["_id"]}')

    assert resp.status == 403

    auth = get_auth(client, user)

    resp = await client.get(f'/api/aa/{instance["_id"]}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 403

    auth = get_auth(client, user2)

    resp = await client.get(f'/api/aa/{instance["_id"]}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 403

    resp = await client.get(f'/api/aa', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 403

    root = dict(**user2, is_staff=True)

    auth = get_auth(client, root)

    resp = await client.get(f'/api/aa/{instance["_id"]}', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200

    resp = await client.get(f'/api/aa', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200
