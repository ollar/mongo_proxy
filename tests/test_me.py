from json import dumps
from .fixtures import client
import jwt
from app.errors import FORBIDDEN


staff = {
    "id": 1,
    "name": "staff",
    "is_staff": True
}

user_auth = {
    "user_id": 1,
    "name": "tester"
}

user2_auth = {
    "user_id": 2,
    "name": "tester2"
}

user = {
    "name": "tester",
    "lalal": "ooo"
}

user2 = {
    "name": "tester2",
    "oppa": "loppa"
}


def get_auth(client, user):
    configs = client.app['configs']
    return jwt.encode(user, configs.secret).decode()


async def test_smoke(client):
    resp = await client.get('/me?collection_name=test')

    assert resp.status == 403
    text = await resp.text()
    assert FORBIDDEN in text


async def test_get_me(client):
    auth = get_auth(client, user_auth)

    await client.post('/api/test/users', data=dumps(user), headers={
        "Authorization": f"Bearer {auth}"
    })

    resp = await client.get('/me?collection_name=test', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200
    json = await resp.json()

    assert json['name'] == user['name']
    assert json['lalal'] == user['lalal']


async def test_get_not_me(client):
    auth = get_auth(client, user_auth)
    await client.post('/api/test/users', data=dumps(user), headers={
        "Authorization": f"Bearer {auth}"
    })

    auth = get_auth(client, user2_auth)
    await client.post('/api/test/users', data=dumps(user2), headers={
        "Authorization": f"Bearer {auth}"
    })

    auth = get_auth(client, user_auth)

    resp = await client.get('/me?collection_name=test', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200
    json = await resp.json()

    assert json['name'] != user2['name']
    assert json['_uid'] != user2_auth['user_id']


async def test_get_me_no_db_entry(client):
    auth = get_auth(client, user_auth)

    resp = await client.get('/me?collection_name=test', headers={
        "Authorization": f"Bearer {auth}"
    })

    assert resp.status == 200