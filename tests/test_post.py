import json

from .fixtures import client

from app.errors import COLLECTION_NAME_INTERFERES_WITH_RELATIVE_COLLECTION_NAME, INVALID_COLLECTION_NAME


dumps = json.dumps

data = dumps({
    "test": True
})

async def test_post_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.post('/api/aa', data=data)
    assert resp.status == 201

async def test_post_empty(client):
    """
    Tests app rejects posts on empty urls
    """
    resp = await client.post('/api/', data=data)
    assert resp.status == 400
    assert INVALID_COLLECTION_NAME in await resp.text()

async def test_post_url_matches_collection_name(client):
    """
    check if post url matches some collection's entity url
    e.g. collection 'aaa' and post to 'aaa/bbb' should be rejected
    """
    resp = await client.post('/api/aa1', data=data)
    assert resp.status == 201

    resp = await client.post('/api/aa1/bb2', data=data)

    assert resp.status == 400
    assert COLLECTION_NAME_INTERFERES_WITH_RELATIVE_COLLECTION_NAME in await resp.text()

async def test_different_urls(client):
    resp = await client.post('/api/555', data=data)
    assert resp.status == 201

    resp = await client.post('/api/œ˙´®ß¯ðƒ///!@#$', data=data)
    assert resp.status == 201

    resp = await client.post('/api/не надо так', data=data)
    assert resp.status == 201

    resp = await client.post('/api/   ', data=data)
    assert resp.status == 201

async def test_post_multiple_documents(client):
    multi_docs = [{"number": i} for i in range(10)]

    resp = await client.post('/api/aa', data=dumps(multi_docs))
    assert resp.status == 201
    resp_json = await resp.json()
    assert(len(resp_json) == 10)
