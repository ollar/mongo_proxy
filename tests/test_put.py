from .fixtures import client

import json
from bson import ObjectId

from app.errors import INVALID_ENTITY_ID, INVALID_COLLECTION_NAME

dumps = json.dumps

data = dumps({
    "test": True
})

async def test_put_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.post('/api/aa', data=data)

    result = await resp.text()
    result = json.loads(result)
    _id = result["_id"]

    resp = await client.put(f'/api/aa/{_id}', data=dumps({
        "test": False
    }))

    assert resp.status == 200

async def test_put_empty(client):
    """
    Tests app rejects put on empty urls
    """
    resp = await client.put('/api/', data=data)
    assert resp.status == 400

async def test_put_with_invalid_id(client):
    """
    check if put url with invalid id rejected
    """
    resp = await client.put('/api/aa1/bb2', data=data)

    assert resp.status == 400
    assert INVALID_ENTITY_ID in await resp.text()

async def test_put_with_any_id(client):
    """
    check if put url with any passes
    """
    id = str(ObjectId())

    resp = await client.put(f'/api/aa1/{id}', data=data)

    assert resp.status == 200

async def test_put_with_plain_url(client):
    """
    check if put on 1 level url
    e.g. /aaa
    """
    resp = await client.put(f'/api/aaa', data=data)

    assert resp.status == 400

async def test_different_urls(client):
    id = str(ObjectId())

    resp = await client.put(f'/api/555/{id}', data=data)
    assert resp.status == 200

    resp = await client.put(f'/api/œ˙´®ß¯ðƒ&/{id}', data=data)
    assert resp.status == 200

    resp = await client.put(f'/api/не надо так/{id}', data=data)
    assert resp.status == 200

    resp = await client.put(f'/api/   /{id}', data=data)
    assert resp.status == 200

    resp = await client.put(f'/api/œ˙´®ß¯ðƒ$//{id}', data=data)
    assert resp.status == 400
    assert INVALID_COLLECTION_NAME in await resp.text()