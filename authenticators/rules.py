from urllib.parse import quote


class DbStoredRules:
    table_prefix = '__access_rules__/'


    def __init__(self, adapter, configs):
        self.adapter = adapter
        self.configs = configs


    async def get_rule_for_request(self, request, path):
        if self.table_prefix in path:
            return 'staff'

        access_rules = await self.adapter._find_all(self.table_prefix + quote(path), request=request)

        if len(access_rules) == 0:
            return None

        access_rule = access_rules[0]
        return access_rule.get(request.method)


