from urllib.parse import quote
from functools import wraps
from jwt.exceptions import ExpiredSignatureError, InvalidSignatureError, DecodeError

from authenticators import Authenticator, AccessRestictedException
from utils import parse_request_path, _get_user_from_bearer, _user_is_authorized
from app.errors import TOKEN_EXPIRED, TOKEN_INVALID


class JwtException(Exception):
    """ Exception raised on jwt token decoding """


def jwt_errors_handler(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            result = await func(*args, **kwargs)
        except ExpiredSignatureError:
            raise JwtException(TOKEN_EXPIRED)
        except InvalidSignatureError:
            raise JwtException(TOKEN_INVALID)
        except DecodeError:
            raise JwtException(TOKEN_INVALID)

        return result

    return wrapper


class JWTAuthenticator(Authenticator):
    SECURITY_HEADER_KEY_PREFIX = 'Bearer '


    def __init__(self, adapter, configs, access_rules):
        self.adapter = adapter
        self.configs = configs
        self.access_rules = access_rules


    async def get_access_rules(self, request, path):
        return await self.access_rules.get_rule_for_request(request, path)


    @jwt_errors_handler
    async def get_auth_data(self, request):
        bearer = self.get_access_token(request)
        auth_data = None

        if bearer:
            auth_data = _get_user_from_bearer(bearer, self.configs.secret)
        return auth_data


    @jwt_errors_handler
    async def access_granted(self, request):
        bearer = self.get_access_token(request)
        _, collection_full_name, entity_id, collection_list_name = parse_request_path(request)
        request_to_entity = collection_list_name and await self.adapter.collection_exists(collection_list_name) and entity_id

        path = collection_full_name
        if request_to_entity:
            path = collection_list_name

        access_rule = await self.get_access_rules(request, path)


        if not access_rule:
            return True


        if access_rule == 'authorized':
            if _user_is_authorized(bearer, self.configs.secret):
                return True


        if access_rule == 'owner':
            if _user_is_authorized(bearer, self.configs.secret):
                if request_to_entity:
                    user = _get_user_from_bearer(bearer, self.configs.secret)
                    entity = await self.adapter._find_record(collection_list_name, entity_id, request=request)

                    if user.get('user_id') == entity.get('_uid'):
                        return True

                else:
                    # todo check this
                    return True


        if access_rule == 'staff':
            if _user_is_authorized(bearer, self.configs.secret):
                user = _get_user_from_bearer(bearer, self.configs.secret)
                if user.get('is_staff', False) == True:
                    return True


        if access_rule == 'superuser':
            if _user_is_authorized(bearer, self.configs.secret):
                user = _get_user_from_bearer(bearer, self.configs.secret)
                if user.get('is_superuser', False) == True:
                    return True


        raise AccessRestictedException
