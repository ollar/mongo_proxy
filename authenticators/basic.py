import base64
from dataclasses import dataclass
from .base import Authenticator


@dataclass
class User:
    username: str
    password: str


class BasicAuthenticator(Authenticator):
    SECURITY_HEADER_KEY_PREFIX = 'Basic '


    @staticmethod
    def _get_credentials_from_token(token):
        credentials = base64.b64decode(token.encode()).decode()
        username, password = credentials.split(':')
        return (username, password)


    def get_access_rules(self):
        """TODO place real rules for accessing resources"""
        pass


    def access_granted(self, request):
        token = self.get_access_token(request)
        username, password = self._get_credentials_from_token(token)
        user = User('test', 'pass')

        return user.username == username and user.password == password