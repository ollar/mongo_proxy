from .base import Authenticator

class SecretKeyAuthenticator(Authenticator):
    SECURITY_HEADER_KEY_PREFIX = 'secret_key '


    def __init__(self, secret_key):
        self.secret_key = secret_key


    def get_access_rules(self):
        pass


    def access_granted(self, request):
        return self.get_access_token(request) == self.secret_key