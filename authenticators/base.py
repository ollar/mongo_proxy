class AccessRestictedException(Exception):
    """ Exception raised on access rules check """


class Authenticator:
    SECURITY_HEADER_KEY = 'Authorization'
    SECURITY_HEADER_KEY_PREFIX = ''


    def __init__(self):
        pass


    def get_access_token(self, request):
        return request.headers.get(self.SECURITY_HEADER_KEY, '')\
            .replace(self.SECURITY_HEADER_KEY_PREFIX, '')


    def get_access_rules(self):
        pass


    def access_granted(self):
        pass