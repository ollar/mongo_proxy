from aiohttp import web
from utils import json_response
from authenticators import AccessRestictedException


routes = web.RouteTableDef()


@routes.get('')
async def get_handler(request):
    collection_name = request.query.get('collection_name')
    adapter = request.app['adapter']
    authenticator = request.app['authenticator']

    auth_data = await authenticator.get_auth_data(request)

    if not auth_data:
        raise AccessRestictedException

    user_id = auth_data.get('user_id')

    if not user_id:
        raise AccessRestictedException

    result = await adapter._find_all(f"{collection_name}/users", find={"_uid": user_id}, request=request)

    if result and len(result) > 0:
        return json_response(data=result[0])
    else:
        result = await adapter.create_record(f"{collection_name}/users", data={}, request=request)
        return json_response(data=result)

    raise AccessRestictedException


def getRoutes():
    return routes
