from functools import singledispatch

@singledispatch
def serialize_response(arg):
    return arg


@serialize_response.register(dict)
def _(data):
    return {"data": data}


@serialize_response.register(list)
def _(data):
    return {"data": data}
