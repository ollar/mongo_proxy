from aiohttp import web
from functools import wraps

from utils import json_response
from authenticators import JWTAuthenticator, AccessRestictedException
from authenticators.jwt import JwtException
from .errors import FORBIDDEN


# catch access errors
def access_errors_handler(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        error = lambda e: json_response(data={ "error": str(e) }, status=403)

        try:
            return await func(*args, **kwargs)
        except JwtException as e:
            return error(e)
        except AccessRestictedException as e:
            return error(FORBIDDEN)

    return wrapper


@web.middleware
async def cors_middleware(request, handler):
    configs = request.app['configs']

    headers = {
        'Access-Control-Allow-Origin': configs.app_headers_allow_origin,
        'Access-Control-Allow-Credentials': configs.app_headers_allow_credentials,
        'Access-Control-Allow-Headers': configs.app_headers_allow_headers,
        'Access-Control-Allow-Methods': configs.app_headers_allow_methods,
    }

    if request.method == 'OPTIONS':
        response = web.Response()
    else:
        response = await handler(request)
    response.headers.update(headers)

    return response


@web.middleware
@access_errors_handler
async def access_check_middleware(request, handler):
    return await handler(request)
