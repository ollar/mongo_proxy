from aiohttp import web
import uvloop
import asyncio
import logging

from .configs import Configs
from .db import initDB
from .routes import getRoutes
from .middlewares import cors_middleware, access_check_middleware
from adapters.app import AppAdapter
from authenticators import JWTAuthenticator
from authenticators.rules import DbStoredRules

from me.main import make_app as make_me_app
from burst_request.main import make_app as make_burst_request_app

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

logging.basicConfig(level=logging.INFO)

async def make_app(**options):
    configs = Configs(**options)
    app = web.Application(
        middlewares=[
            cors_middleware,
            access_check_middleware,
        ]
    )
    app['configs'] = configs
    app.add_routes(getRoutes())

    me_app = await make_me_app()
    # burst_request_app = await make_burst_request_app()

    app.add_subapp('/me/', me_app)
    # app.add_subapp('/burst/', burst_request_app)

    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)

    return app

async def on_startup(app):
    logging.info('App is starting up')

    (mongo_client, db) = await initDB(app['configs'])
    app['adapter'] = AppAdapter(db, configs=app['configs'])
    app['access_rules'] = DbStoredRules(adapter=app['adapter'], configs=app['configs'])
    app['authenticator'] = JWTAuthenticator(adapter=app['adapter'], configs=app['configs'], access_rules=app['access_rules'])
    app['mongo_client'] = mongo_client

    for subapp in app._subapps:
        subapp['configs'] = app['configs']
        subapp['adapter'] = app['adapter']
        subapp['access_rules'] = app['access_rules']
        subapp['authenticator'] = app['authenticator']
        subapp['mongo_client'] = app['mongo_client']


async def on_cleanup(app):
    logging.info('App is cleaning up')
    app['mongo_client'].close()
