from aiohttp import web
from functools import wraps

from bson.errors import InvalidId
from pymongo.errors import InvalidName
from adapters.base import RequestNotAcknowledged

from utils import json_response, \
                    parse_request_path, \
                    parse_query

from app.errors import INVALID_COLLECTION_NAME,\
                    COLLECTION_NAME_INTERFERES_WITH_RELATIVE_COLLECTION_NAME, \
                    INVALID_ENTITY_ID, \
                    REQUEST_NOT_ACKNOWLEDGED


routes = web.RouteTableDef()


class CollectionNameInterferes(Exception):
    """collection name interferes with relative collection name"""


def mongo_errors_handler(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            result = await func(*args, **kwargs)
        except InvalidName:
            return json_response(data={
                "error": INVALID_COLLECTION_NAME
            }, status=400)
        except InvalidId:
            return json_response(data={
                "error": INVALID_ENTITY_ID
            }, status=400)

        except RequestNotAcknowledged:
            return json_response(data={
                "error": REQUEST_NOT_ACKNOWLEDGED
            }, status=500)
        except CollectionNameInterferes:
            return json_response(data={
                "error": COLLECTION_NAME_INTERFERES_WITH_RELATIVE_COLLECTION_NAME
            }, status=400)

        return result

    return wrapper


@routes.get('/api/{path:.*}')
@mongo_errors_handler
async def get_handler(request):
    adapter = request.app['adapter']

    _, collection_full_name, entity_id, collection_list_name = parse_request_path(request)

    result = {}

    if collection_list_name and await adapter.collection_exists(collection_list_name):
        # take entity by id

        result = await adapter.find_record(collection_list_name, entity_id, request=request)

        if not result:
            return json_response(data=result, status=404)

        return json_response(data=result)

    else:
        # take all collection

        find, sort, pagination = parse_query(request.query)

        result = await adapter.find_all(collection_name=collection_full_name, find=find, sort=sort, pagination=pagination, request=request)

        return json_response(data=result, status=200)


@routes.post('/api/{path:.*}')
@mongo_errors_handler
async def post_handler(request):
    adapter = request.app['adapter']

    _, collection_full_name, _, collection_list_name = parse_request_path(request)

    """
    check if post url matches some collection's entity url
    e.g. collection 'aaa' and post to 'aaa/bbb' should be rejected
    """
    if collection_list_name and \
        await adapter.collection_exists(collection_list_name):
        raise CollectionNameInterferes

    data = await request.json()

    result = await adapter.create_record(collection_full_name, data, request=request)

    return json_response(data=result, status=201)


@routes.put('/api/{path:.*}')
@mongo_errors_handler
async def put_handler(request):
    adapter = request.app['adapter']

    _, _, entity_id, collection_list_name = parse_request_path(request)

    data = await request.json()

    result = await adapter.update_record(collection_list_name, entity_id, data, request=request)

    return json_response(data=result, status=200)


@routes.patch('/api/{path:.*}')
@mongo_errors_handler
async def patch_handler(request):
    adapter = request.app['adapter']

    _, _, entity_id, collection_list_name = parse_request_path(request)

    data = await request.json()

    result = await adapter.patch_record(collection_list_name, entity_id, data, request=request)

    return json_response(data=result, status=200)


@routes.delete('/api/{path:.*}')
@mongo_errors_handler
async def delete_handler(request):
    adapter = request.app['adapter']

    _, _, entity_id, collection_list_name = parse_request_path(request)

    result = await adapter.delete_record(collection_list_name, entity_id, request=request)

    return json_response(data=result, status=200)


def getRoutes():
    return routes