from motor import motor_asyncio

async def initDB(configs):
    mongo_client = motor_asyncio.AsyncIOMotorClient(host=configs.mongo_host, port=configs.mongo_port)

    if configs.mongo_drop_table_on_init:
        await mongo_client.drop_database(configs.mongo_db_name)

    db = mongo_client[configs.mongo_db_name]
    return (mongo_client, db)
