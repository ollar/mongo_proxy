import asyncio
from aiohttp import web
from utils import json_response, extract_parameter
from app.errors import REQUEST_NOT_ACKNOWLEDGED
from json import dumps


routes = web.RouteTableDef()


class RequestFake():
    method = 'GET'

    def __init__(self, request, path):
        self.app = request.app
        self.headers = request.headers
        self.match_info = {
            "path": path
        }


@routes.get('')
async def get_handler(request):
    burst_requests = []
    response = json_response(data={
        "error": REQUEST_NOT_ACKNOWLEDGED
    }, status=400)

    adapter = request.app['adapter']
    query = extract_parameter('query', request.query)

    for entity in query:
        for entity_name, entity_search in entity.items():
            if entity_search.get('id'):
                burst_requests.append(adapter.find_record(entity_name, entity_search['id'], request=RequestFake(request=request, path=f"{entity_name}/{entity_search['id']}")))
            elif entity_search.get('find'):
                burst_requests.append(adapter.find_all(entity_name, entity_search.get('find'), entity_search.get('sort'), entity_search.get('pagination'), request=RequestFake(request=request, path=f"{entity_name}")))


    results = await asyncio.gather(*burst_requests)

    if results:
        results = [item for item in results if item]
        return json_response(data=results, status=200)

    return response


def getRoutes():
    return routes
