from bson import ObjectId
from functools import wraps
from .base import Adapter, DatabaseRequired, RequestNotAcknowledged
from pymongo.results import InsertOneResult, InsertManyResult


def adapter_auth_check(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        request = kwargs.get('request')

        authenticator = request.app['authenticator']
        await authenticator.access_granted(request)

        return await func(authenticator=authenticator, *args, **kwargs)

    return wrapper


class AppAdapter(Adapter):
    def __init__(self, db, configs):
        if not db: raise DatabaseRequired
        self.db = db
        self.configs = configs


    async def collection_exists(self, collection_list_name):
        return collection_list_name in await self.db.list_collection_names()


    async def _find_record(self, collection_name, entity_id, **kwargs):
        return await self.db[collection_name].find_one({"_id": ObjectId(entity_id)})


    async def _find_all(self, collection_name, find:dict={}, sort:list=None, pagination=None, **kwargs):
        result = self.db[collection_name]\
                    .find(find)

        if sort:
            result = result.sort(sort)

        if pagination:
            result = result.skip((pagination - 1) * self.configs.list_length)
        return await result.to_list(length=self.configs.list_length)
        # return await result.to_list(None)


    async def _create_record(self, collection_name, data: object, **kwargs):
        request = kwargs.get('request')
        authenticator = kwargs.get('authenticator')
        result = None

        auth_data = await authenticator.get_auth_data(request)

        if auth_data and auth_data.get('user_id'):
            data['_uid'] = auth_data['user_id']

        if type(data) is list:
            _mongo_res = await self.db[collection_name].insert_many(data)
        else:
            _mongo_res = await self.db[collection_name].insert_one(data)

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        if type(_mongo_res) is InsertOneResult:
            result = await self._find_record(collection_name, _mongo_res.inserted_id, request=request)
        elif type(_mongo_res) is InsertManyResult:
            result = [await self._find_record(collection_name, _id, request=request) for _id in _mongo_res.inserted_ids]

        return result


    async def _update_record(self, collection_name, entity_id, data, **kwargs):
        request = kwargs.get('request')
        _current_record =await self.db[collection_name].find_one({"_id": ObjectId(entity_id)})

        if _current_record:
            data['_uid'] = _current_record.get('_uid')

        _mongo_res = await self.db[collection_name].replace_one({"_id": ObjectId(entity_id)}, data)

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return await self._find_record(collection_name, entity_id, request=request)


    async def _patch_record(self, collection_name, entity_id, data, **kwargs):
        authenticator = kwargs.get('authenticator')
        request = kwargs.get('request')
        auth_data = await authenticator.get_auth_data(request)

        if auth_data and auth_data['user_id']:
            data['_uid'] = auth_data['user_id']

        _mongo_res = await self.db[collection_name].update_one(
            {"_id": ObjectId(entity_id)},
            {"$set": data}
        )

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return await self._find_record(collection_name, entity_id, request=request)


    async def _delete_record(self, collection_name, entity_id, **kwargs):
        _mongo_res = await self.db[collection_name].delete_one({"_id": ObjectId(entity_id)})

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return { "result": _mongo_res.acknowledged }


    @adapter_auth_check
    async def find_record(self, collection_name, entity_id, **kwargs):
        return await self._find_record(collection_name, entity_id, **kwargs)

    @adapter_auth_check
    async def find_all(self, collection_name, find:dict={}, sort:list=None, pagination=None, **kwargs):
        return await self._find_all(collection_name, find, sort, pagination, **kwargs)

    @adapter_auth_check
    async def create_record(self, collection_name, data: object, **kwargs):
        return await self._create_record(collection_name, data, **kwargs)

    @adapter_auth_check
    async def update_record(self, collection_name, entity_id, data, **kwargs):
        return await self._update_record(collection_name, entity_id, data, **kwargs)

    @adapter_auth_check
    async def patch_record(self, collection_name, entity_id, data, **kwargs):
        return await self._patch_record(collection_name, entity_id, data, **kwargs)

    @adapter_auth_check
    async def delete_record(self, collection_name, entity_id, **kwargs):
        return await self._delete_record(collection_name, entity_id, **kwargs)